
const txtNombre=document.getElementById('idnombre').value;
const txtMensage=document.getElementById('idsalida');

const btnenviar=document.getElementById('btnenviar');
const btnlimpiar=document.getElementById('btnlimpiar');
const btncalcular=document.getElementById('btncalcular');

btnenviar.addEventListener('click',mostrar);
btnlimpiar.addEventListener('click',limpiar);




function mostrar(){
    const txtNombre=document.getElementById('idnombre').value;
    const txtMensage=document.getElementById('idsalida');
    txtMensage.value=txtNombre;
}
btnlimpiar.addEventListener('click',limpiar)

function limpiar(){
    const txtNombre=document.getElementById('idnombre');
    const txtMensage=document.getElementById('idsalida');
    txtMensage.value="";
    txtNombre.value="";
}
document.getElementById('btncalcular').addEventListener('submit', function(event) {
    event.preventDefault();
    var peso = parseFloat(document.getElementById('idpeso').value);
    var altura = parseFloat(document.getElementById('idaltura').value);
    var imc = peso / (altura * altura);
    alert('Tu IMC es: ' + imc.toFixed(2));
});
function calculateBMI() {
    var weight = parseFloat(document.getElementById('weight').value);
    var height = parseFloat(document.getElementById('height').value);

    if (isNaN(weight) || isNaN(height) || weight <= 0 || height <= 0) {
        document.getElementById('result').innerHTML = 'Por favor, introduzca valores válidos.';
        return;
    }

    var bmi = weight / (height * height);
    var result = 'Su IMC es ' + bmi.toFixed(2) + '. ';

    if (bmi < 18.5) {
        result += 'Está bajo de peso.';
    } else if (bmi < 25) {
        result += 'Tiene un peso saludable.';
    } else if (bmi < 30) {
        result += 'Tiene sobrepeso.';
    } else {
        result += 'Tiene obesidad.';
    }

    document.getElementById('result').innerHTML = result;
}
function clearFields() {
    document.getElementById('weight').value = '';
    document.getElementById('height').value = '';
    document.getElementById('result').innerHTML = '';
}